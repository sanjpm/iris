This project was created to experiment with computer vision for the purposes of analyzing a roof for moisture intrusion.

Input images are example captures from thermal cameras that were mounted on drones.

The goal was to output data that estimated the total area of moisture damage.

Basic program flow is as follows:
1. A listener thread waits for a JSON object with a web address to an image to be analyzed.
2. The JSON library decodes the data and passes the address to the cURL library.
3. The cURL library downloads a copy of the image file to memory.
4. The OpenCV library converts the image to a greyscale image and looks for contours.
5. The program returns total contoured area (the problem spots on the roof) and total area in pixels to a web client in JSON format.

There are a few example images in the earlier commits. Future improvements would probably relate to improving detection accuracy, and converting pixels to a unit of surface area. This project is still very much a proof-of-concept.

Questions, comments, or ideas? -> sanjay at eneris dot ca