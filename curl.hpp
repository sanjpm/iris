/*
 * curl.hpp
 *
 *  Created on: 2017-10-31
 *      Author: sanjay
 */

#ifndef CURL_HPP_
#define CURL_HPP_

#include "curl/curl.h"
#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>

size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata);
cv::Mat curlImg(const char *img_url, int timeout);

#endif /* CURL_HPP_ */
