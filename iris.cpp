/*
 * iris.cpp
 *	File for eneris specific opencv algorithms
 *  Created on: 2017-10-31
 *      Author: sanjay
 */

#include "iris.hpp"
#include "curl.hpp"
#include <math.h>

using namespace cv;
using namespace std;
using json = nlohmann::json;

int cv_analyze (string imgPath){

	report imgReport;
	Mat im, gray, original, blended;

	double minContourArea = 50;	//original 50
	double maxContourArea = 40000; //original 1000
	double area = 0;
	int totalAffectedArea = 0;

	im = curlImg(imgPath.c_str(), 10);
	original = im;

	imgReport.set_imgPath(imgPath);
	cout << "Input Image: " << im.size().width << " x " << im.size().height << endl;
	cout << "Total Area (px): " << (im.size().width * im.size().height) << endl;
	imgReport.set_imgWidth(im.size().width);
	imgReport.set_imgHeight(im.size().height);
	imgReport.set_totalArea(im.size().height * im.size().width); // add sqft per px scale later


	cvtColor (im, im, CV_BGR2GRAY);
	threshold(im, gray, 82, 255, 0);

	//Contouring example
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	RNG rng(12345);
	findContours(gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(0,0));

	//Draw Contours
	Mat drawing = Mat::zeros(gray.size(),CV_8UC3);

	for ( int i = 0; i < contours.size(); i++)
	{
		Scalar color = Scalar (rng.uniform(0,255), rng.uniform(0,255));
		area = contourArea(contours[i]);
		if(area <= maxContourArea && area >= minContourArea) //restrict contour sizes before adding
		{
			drawContours (drawing, contours, i, color, 1, 8, hierarchy, 0, Point());
			totalAffectedArea += area;
		}
	}

	cout << "Problem Area: " << totalAffectedArea << endl;

	imgReport.set_totalContouredArea(totalAffectedArea);
	set_reportString(imgReport.generate_reportJSON());

	//addWeighted(drawing, 0.5, original, 0.5, 0.0, blended); //<- this is borked for certain images.. need to fix
	//imshow("Result window", drawing);
	//waitKey(0);

	return 0;
}

report::report(){
	//put constructor stuff here later..
}

string report::generate_reportJSON(void){

	json report;
	stringstream ss;

	report["path"] = this->imgPath;
	report["totalArea"] = this->totalArea;
	report["totalContouredArea"] = this->totalContouredArea;

	ss << report;

	return ss.str();
}

namespace {
	std::string rep;
}

void set_reportString(string s){
	rep = s;
}

string get_reportString(void){
	return rep;
}



