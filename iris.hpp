/*
 * iris.hpp
 *
 *  Created on: 2017-10-31
 *      Author: sanjay
 */

#ifndef IRIS_HPP_
#define IRIS_HPP_

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include "json.hpp"

extern int cv_analyze (std::string imgPath);
extern void set_reportString (std::string s);
extern std::string get_reportString(void);

class report {
	public:
		std::string generate_reportJSON(void);
		void set_imgPath (std::string path) { imgPath = path; }
		void set_totalArea(double sqft){ totalArea = sqft; }
		void set_imgWidth(unsigned int width){ imgWidth = width; }
		void set_imgHeight(unsigned int height){ imgHeight = height; }
		void set_totalContouredArea(unsigned int sqft){ totalContouredArea = sqft; }
		report();

	private:
		unsigned int totalArea;
		unsigned int imgWidth;
		unsigned int imgHeight;
		unsigned int totalContouredArea;
		std::string imgPath;

};


#endif /* IRIS_HPP_ */

