#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include "tcp_server.hpp"
#include <pthread.h>
#include "curl.hpp"
#include <csignal>

#define VERSION_NUMBER "1.0.0"
#define TCP_SERVER_PORT 5454

void signalHandler (int signnum);

int main (int argc, char** argv)
{
	signal(SIGINT, signalHandler); //register signal handler.

	std::cout << "Eneris CV Server " << VERSION_NUMBER << " started. Listening on port:" << TCP_SERVER_PORT << std::endl;

	try
	{
		boost::asio::io_service io_service;
		server(io_service, TCP_SERVER_PORT);
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}

void signalHandler (int signum) {
	//get current time
	time_t rawtime = time(0);

	std::cout << "Interrupt signal " << signum << " received." << std::endl;
	std::cout << ctime(&rawtime) << "Eneris CV Server stopping..." << std::endl;
	exit (signum);
}
