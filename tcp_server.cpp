//
// blocking_tcp_echo_server.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2017 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <cstdlib>
#include <iostream>
#include <thread>
#include <utility>
#include <boost/asio.hpp>
#include <string>
#include <sstream>
#include <pthread.h>
#include "json.hpp"
#include "tcp_server.hpp"
#include "iris.hpp"

using boost::asio::ip::tcp;
using json = nlohmann::json;

const int max_length = 1024;

tcpstream *tcpstream::s_instance;

void session(tcp::socket sock)
{
	std::string imgPath;

  try
  {
    for (;;)
    {
      char data[max_length]={};

      boost::system::error_code error;
      size_t length = sock.read_some(boost::asio::buffer(data), error);
      tcpstream::instance()->setClientData(data);

      if (error == boost::asio::error::eof)
      {
    	  break; // Connection closed cleanly by peer.
      }
      else if (error)
      {
          throw boost::system::system_error(error); // Some other error.
      }

      if(tcpstream::instance()->getClientData()[0] == '{'){
    	  auto input = json::parse(tcpstream::instance()->getClientData());
    	  auto path = input.find("path");
    	  if(path != input.end()){
    		  imgPath = *path;
    		  cv_analyze(imgPath);
    		  tcpstream::instance()->setClientData("");
    		  std::cout<<imgPath<<std::endl;
    		  std::fflush(stdout);
    	  }
    	  else {
    		  std::cout<<"no path found"<<std::endl;
    		  set_reportString("invalid input");
    	  }
      }

      std::string response = get_reportString();
      size_t reslen = response.length();
      boost::asio::write(sock, boost::asio::buffer(response, reslen));

    }
  }
  catch (std::exception& e)
  {
    std::cerr << "Exception in thread: " << e.what() << "\n";
  }
}

void server(boost::asio::io_service& io_service, unsigned short port)
{
  tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));
  for (;;)
  {
    tcp::socket sock(io_service);
    a.accept(sock);
    std::thread(session, std::move(sock)).detach();
  }
}

