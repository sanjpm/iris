/*
 * tcp_server.hpp
 *
 *  Created on: 2017-10-24
 *      Author: sanjay
 */

#ifndef TCP_SERVER_HPP_
#define TCP_SERVER_HPP_

#include <boost/asio.hpp>

void session(boost::asio::ip::tcp::socket sock);

void server(boost::asio::io_service& io_service, unsigned short port);

class tcpstream
{
	static tcpstream *s_instance;

	public:
		std::string getClientData(void){ return data; }
		void setClientData(std::string arr){ data = arr; }
		static tcpstream *instance()
		{
			if(!s_instance)
				s_instance = new tcpstream;
			return s_instance;
		}

	private:
		std::string data;
};

#endif /* TCP_SERVER_HPP_ */
